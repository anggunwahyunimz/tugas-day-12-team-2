const Geometry = require('./geometry');

class ThreeDimention extends Geometry {
  constructor(name) {
    super(name, 'Three Dimention');

    if (this.constructor === ThreeDimention) {
      throw new Error('Cannot instantiate from Abstract Class');
    }
  }

  calculateArea() {
    console.log(`Calculate ${this.name} Area!`);
  }

  calculateVolume() {
    console.log(`Calculate ${this.name} Volume!`);
  }
}

module.exports = ThreeDimention;
