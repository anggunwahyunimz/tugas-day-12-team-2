exports.Rectangle = require('./rectangle');
exports.Square = require('./square');
exports.Triangle = require('./triangle');

exports.Tube = require('./tube');
exports.Cube = require('./cube');
exports.Beam = require('./beam');
