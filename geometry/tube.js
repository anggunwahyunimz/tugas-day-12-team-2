const ThreeDimention = require('./threeDimention');

class Tube extends ThreeDimention {
  constructor(radius, height) {
    super('Tube');
    this.radius = radius;
    this.height = height;
  }

  calculateArea() {
    super.calculateArea();
    return Math.PI * Math.pow(this.radius, 2);
  }

  calculateVolume() {
    super.calculateVolume();
    return Math.PI * Math.pow(this.radius, 2) * this.height;
  }
}

module.exports = Tube;
